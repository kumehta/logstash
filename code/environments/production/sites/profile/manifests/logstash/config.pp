# This class manages configuration directories for Logstash.
#
# @example Include this class to ensure its resources are available.
#   include logstash::config
#
# maintainer Kunal Mehta	<kunal.a.mehta@capgemini.com>
#
class profile::logstash::config {


  $ensure     = lookup('profile::logstash::ensure', String, deep)
  $config_dir = lookup('profile::logstash::config_dir', String, deep)
  $purge_config = lookup('profile::logstash::purge_config', Boolean, deep)
  
  File {
    owner => 'root',
    group => 'root',
  }

  # Configuration "fragment" directories for pipeline config and pattern files.
  # We'll keep these seperate since we may want to "purge" them. It's easy to
  # end up with orphan files when managing config fragments with Puppet.
  # Purging the directories resolves the problem.

  if($ensure == 'present') {
    file { $config_dir:
      ensure => directory,
      mode   => '0755',
    }

    file { "$config_dir/conf.d":
      ensure  => directory,
      purge   => $purge_config,
      recurse => $purge_config,
      mode    => '0775',
      notify  => Service['logstash'],
    }

    file {     "$config_dir/patterns":
      ensure  => directory,
      purge   => $purge_config,
      recurse => $purge_config,
      mode    => '0755',
    }
  }
  elsif($ensure == 'absent') {
    # Completely remove the config directory. ie. 'rm -rf /etc/logstash'
    file { $config_dir:
      ensure  => 'absent',
      recurse => true,
      force   => true,
    }
  }
}

