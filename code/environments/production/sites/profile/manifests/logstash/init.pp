# Initial configuration file for Logstash profile
# This profile has been implemented using Hiera
# Logstash base module hass been used from forge.puppet.com
# Maintainer Kunal Mehta <kunal.a.mehta@capgemini.com>
#
class profile::logstash::init(
  $ensure 			= lookup('profile::logstash::ensure', String, deep),            
  $status 			= lookup('profile::logstash::status', String, deep),             
  $restart_on_change 	        = lookup('profile::logstash::restart_on_change', Boolean, deep),  
  $auto_upgrade 		= lookup('profile::logstash::auto_upgrade', Boolean, deep),       
  $version 		        = lookup('profile::logstash::version', String, deep),            
  $package_url 			= lookup('profile::logstash::package_url', String, deep),        
  $package_name 		= lookup('profile::logstash::package_name', String, deep),       
  $download_timeout 	        = lookup('profile::logstash::download_timeout', Integer, deep),   
  $logstash_user 		= lookup('profile::logstash::logstash_user', String, deep),      
  $logstash_group 		= lookup('profile::logstash::logstash_group', String, deep),     
  $config_dir 			= lookup('profile::logstash::config_dir', String, deep),         
  $purge_config 		= lookup('profile::logstash::purge_config', Boolean, deep),       
  $service_provider 	        = lookup('profile::logstash::service_provider', String, deep),   
  $settings 			= lookup('profile::logstash::settings', Hash, deep),           
  $startup_options 		= lookup('profile::logstash::startup_options', Hash, deep),    
  $jvm_options 			= lookup('profile::logstash::jvm_options', Array, deep),        
  $pipelines 			= lookup('profile::logstash::pipelines', Array, deep),          
  $manage_repo 			= lookup('profile::logstash::manage_repo', Boolean, deep),        
)
{
  $home_dir = lookup('profile::logstash::home_dir', String, deep)

  if ! ($ensure in [ 'present', 'absent' ]) {
    fail("\"${ensure}\" is not a valid ensure parameter value")
  }

  if ! ($status in [ 'enabled', 'disabled', 'running', 'unmanaged' ]) {
    fail("\"${status}\" is not a valid status parameter value")
  }

  if ($manage_repo == true) {
    $package_RHEL_logstash = lookup('profile::logstash::install::package_RHEL_logstash', String, deep)
    $nexus_repo_RHEL = lookup('profile::logstash::install::nexus_repo_RHEL', String, deep)
  
  # Identify the Nexus Repository Server to be used.
    $nexus_server = lookup('profile::common::nexus', String, deep)

  
  
  # Set the global execution path.
  Exec { path => ['/sbin', '/bin', '/usr/sbin', '/usr/bin', '/opt/sftpplus/bin'], }

  # Create the installation packages directory.
  file { "/opt/packages":
    ensure  => directory,
    owner   => '0',
    group   => '0',
    mode    => '0755',
  } ->

# Operating system dependent installation. 
  case $facts['os']['family'] {

    'RedHat': {

      # Download the installation media.
      exec { "Download $package_RHEL_logstash.rpm":
        cwd     => "/opt/packages",
        command => "wget http://13.127.144.138:8080/nexus/service/local/repositories/repo1/content/logstash-6.3.1.rpm",
        unless  => "test -d /opt/$package_RHEL_logstash",
        require => File['/opt/packages'],
      } ->

      # Install the Logstash Repository.
      exec { "$package_RHEL_logstash-Repository":
        command => "rpm -ivh /opt/packages/$package_RHEL_logstash.rpm",
        creates => "/etc/yum.repos.d/logstash.repo",
        require => Exec["Download $package_RHEL_logstash.rpm"],
      } ->

	    # Install the Logstash Repository.
      exec { "$package_RHEL_logstash-Install":
        command => "/usr/bin/yum install logstash",
        creates => "/opt/logstash",
       
      } ->
	  
      # (Re)set application code file permissions.
     # exec { "$package_RHEL_logstash-Permissions":
     #   command => "chown -R $owner:$group /usr/share/logstash && touch /usr/share/logstash/.done.perms",
     #   creates => "/usr/share/logstash/.done.perms",
     # } ->

      # Create directory symlink to this package.
      #file { "/opt/logstash":
       # ensure  => link,
       # replace => yes,
       # target  => "/opt/logstash",
       # owner   => $owner,
       # group   => $group,
      #} ->

      # Link the application start/stop logstash.
      file { "/etc/init.d/logstash":
        ensure  => link,
        replace => yes,
        target  => "/usr/share/logstash/bin/logstash",
        owner   => '0',
        group   => '0',
      } ->

      # Delete the downloaded media file.
      exec { "Delete $package_RHEL_logstash.rpm":
        command => "rm -f /opt/packages/$package_RHEL_logstash.rpm",
        onlyif  => "test -f /opt/packages/$package_RHEL_logstash.rpm",
      }
    }




  }

  }
  #include logstash::package
  #include logstash::config
  #include logstash::service
  
 # include profile::logstash::config
 # include profile::logstash::service
}

