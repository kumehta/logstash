define docker::dockerexample{

#class { docker:  }

docker::image { 'hello-world':
  #image_tag => 'latest',
}

docker::run { 'helloworld':
  image => 'hello-world',
  #command => '/bin/sh -c "whle true; do echo hello world; sleep 1; done"',
}

#docker::run { 'goodbyecruelworld':
#  image => 'ubuntu',
#  command => '/bin/sh -c "whle true; do echo goodbye cruel world; sleep 1; done"',
#}

}
