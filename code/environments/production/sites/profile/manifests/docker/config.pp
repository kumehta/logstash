# == Class: docker::config
#
class profile::docker::config {
  profile::docker::system_user { $::profile::docker::docker_users: }
}
