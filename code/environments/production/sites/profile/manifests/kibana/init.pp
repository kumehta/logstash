#
# Maintainer Kunal Mehta <kunal.a.mehta@capgemini.com>
#
class profile::kibana::init (
  $ensure 			= lookup('profile::kibana::ensure', String, deep),
  $config 			= lookup('profile::kibana::config', Hash, deep),
  $manage_repo 		        = lookup('profile::kibana::manage_repo', Boolean, deep),
  $oss 				= lookup('profile::kibana::oss', Boolean, deep),
  $status 			= lookup('profile::kibana::status', String, deep),
) {

  #contain profile::kibana::install
  #contain profile::kibana::config
  #contain profile::kibana::service

  if $manage_repo {
    
    $package_RHEL_kibana  = lookup('profile::kibana::install::package_RHEL_kibana', String, deep)
    $nexus_repo_RHEL        = lookup('profile::kibana::install::nexus_repo_RHEL', String, deep)
  
  # Identify the Nexus Repository Server to be used.
   

  
  
  # Set the global execution path.
  Exec { path => ['/sbin', '/bin', '/usr/sbin', '/usr/bin', '/opt/sftpplus/bin'], }
  
   # Create the installation packages directory.
  file { "/opt/packages":
    ensure  => directory,
    owner   => '0',
    group   => '0',
    mode    => '0755',
  } ->
  # Operating system dependent installation. 
  case $facts['os']['family'] {

    'RedHat': {

      # Download the installation media.
      exec { "Download $package_RHEL_kibana.rpm":
        cwd     => "/opt/packages",
        command => "wget http://13.232.168.121:8080/nexus/service/local/repositories/repo1/content/kibana-6.3.1-x86_64.rpm",
        unless  => "test -d /opt/$package_RHEL_kibana",
        require => File['/opt/packages'],
      } ->

      # Install the Kibana Repository.
      exec { "$package_RHEL_kibana-Repository":
        command => "rpm -ivh /opt/packages/$package_RHEL_kibana.rpm",
        creates => "/etc/yum.repos.d/kibana.repo",
        require => Exec["Download $package_RHEL_kibana.rpm"],
      } ->

	    # Install the Kibana.
      exec { "$package_RHEL_kibana-Install":
        command => "/usr/bin/yum install kibana",
        creates => "/usr/share/kibana",
        require => Exec["$package_RHEL_kibana-Repository"],
      } ->
	  
      # (Re)set application code file permissions.
     # exec { "$package_RHEL_kibana-Permissions":
     #   command => "chown -R $owner:$group /usr/share/kibana && touch /usr/share/kibana/.done.perms",
     #   creates => "/usr/share/logstash/.done.perms",
     # } ->

      # Create directory symlink to this package.
      #file { "/opt/kibana":
       # ensure  => link,
       # replace => yes,
       # target  => "/opt/kibana",
       # owner   => $owner,
       # group   => $group,
      #} ->

      # Link the application start/stop logstash.
      file { "/etc/init.d/kibana":
        ensure  => link,
        replace => yes,
        target  => "/usr/share/kibana/bin/kibana",
        owner   => '0',
        group   => '0',
      } ->

      # Delete the downloaded media file.
      exec { "Delete $package_RHEL_kibana.rpm":
        command => "rm -f /opt/packages/$package_RHEL_kibana.rpm",
        onlyif  => "test -f /opt/packages/$package_RHEL_kibana.rpm",
      }
    }
  
  
  }

  # Catch absent values, otherwise default to present/installed ordering
#  case $ensure {
 #   'absent': {
  #    Class['profile::kibana::service']
   #   -> Class['profile::kibana::config']
    #  -> Class['profile::kibana::install']
    #}
    #default: {
    #  Class['profile::kibana::install']
    #  -> Class['profile::kibana::config']
    #  ~> Class['profile::kibana::service']
    #}
  #}
}
}
