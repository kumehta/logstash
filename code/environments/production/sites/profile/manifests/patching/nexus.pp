class profile::patching::nexus
{
$varPatch=lookup('profile::patching::nexus', String, 'deep')
case $varPatch {

      
      '0': {

        exec { 'prepatching':
		  command => '/usr/bin/systemctl stop nexus',
		}
      }

      '1': {
        exec { 'postpatching':
		  command => '/usr/bin/systemctl start nexus',
		}
        
           }
    }
 }
