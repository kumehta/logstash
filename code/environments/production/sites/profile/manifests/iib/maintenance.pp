# -----------------------------------------------------------------------------
# Author  = 
# Date    = 
# Version = 1.0 'broker/maintenance.pp'
# Purpose = configure Broker
#
# Externalisation:
# ---------------
# Broker Creation requires following mandatory parameters:
# - profile::generic::iib::create_broker:    parameters_name_in_hiera
# - profile::generic::iib::config_broker:
# -----------------------------------------------------------------------------

class profile::generic::iib::server::config_broker {

  # Get configuration details.
  $boker_name   = hiera('profile::generic::iib::server::create_broker::boker_name')
  $broker_http_port = hiera('profile::generic::iib::server::create_broker::broker_http_port')
  $broker_https_port = hiera('profile::generic::iib::server::config_broker::broker_https_port')
  $broker_web_port = hiera('profile::generic::iib::server::config_broker::broker_web_port')
  $broker_user_name= hiera('profile::generic::iib::server::config_broker::broker_user_name')
  $broker_user_password= hiera('profile::generic::iib::server::config_broker::broker_user_password')
  $broker_group_username= hiera('profile::generic::iib::server::config_broker::broker_group_username')
  $broker_group_password= hiera ('profile::generic::iib::server::config_broker::broker_group_password')

  #Detrming Execution Grouo Names
  $execution_groups= hiera ('profile::generic::iib::server::config_broker::execution_groups')

  # Get IIB configuration details.
  $iib_package   = hiera('profile::generic::iib::server::install::package')
  $iib_directory = hiera('profile::generic::iib::server::install::directory')

  # Determine IIB version to be installed (V.R.M.F).
  $vrmf = "$iib_package".match(/[0-9]+[\.]+[0-9]+[\.]+[0-9]+[\.]+[0-9]+/)
  $iib_version = $vrmf[0]

  # Loop through setting up each queue manager and authorisations in turn.
  $qmgrs = lookup('profile::generic::mq::server::queue_manager_config::qmgrs', Array[Hash], 'deep', undef)
  $qmgrs.each | Hash $attributes | {

  $qmgr     = $attributes['qmgr']
  $subenv   = $attributes['subenv']
  $address  = $attributes['address']
  $port     = $attributes['port']
  $logtype  = $attributes['logtype']
  $plogsize = $attributes['plogsize']
  $slogsize = $attributes['slogsize']

  Exec {path => ['/sbin', '/bin', '/usr/sbin', '/usr/bin'] }

  #bash -c 'source ${iib_directory}/${iib_directory}/server/bin/mqsiprofile' && ##
  ##Kept it commented to be removed after # REVIEW:  ####

  exec { 'chage_broker_web_port':
    command   => "su - wmbadmin -c 'source /home/wmbadmin/.bashrc && ${iib_directory}/iib-${iib_version}/server/bin/mqsichangeproperties ${boker_name} -b webadmin -o HTTPConnector -n port -v ${broker_web_port}'  && touch ${iib_directory}/iib-${iib_version}/.puppet/mqsichangeproperties_web_port.done",
    creates   => "${iib_directory}/iib-${iib_version}/.puppet/mqsichangeproperties_web_port.done",
    } ->

  exec { 'start_broker_services_web_port_exec':
    command   => "su - wmbadmin -c 'source /home/wmbadmin/.bashrc && ${iib_directory}/iib-${iib_version}/server/bin/mqsistart ${boker_name}' && touch ${iib_directory}/iib-${iib_version}/.puppet/mqsistart_web_port_exec.done",
    timeout   => 1200,
    creates   => "${iib_directory}/iib-${iib_version}/.puppet/mqsistart_web_port_exec.done",
    } ->

  exec { 'chage_broker_https_port':
    command   => "su - wmbadmin -c 'source /home/wmbadmin/.bashrc && ${iib_directory}/iib-${iib_version}/server/bin/mqsichangeproperties ${boker_name} -b httplistener -o HTTPSConnector -n port -v ${broker_https_port}' && touch ${iib_directory}/iib-${iib_version}/.puppet/mqsichangeproperties_https.done",
    creates    => "${iib_directory}/iib-${iib_version}/.puppet/mqsichangeproperties_https.done",
    } ->

  exec { 'enable_ssl_in_broker':
    command   => "su - wmbadmin -c 'source /home/wmbadmin/.bashrc && ${iib_directory}/iib-${iib_version}/server/bin/mqsichangeproperties ${boker_name} -b webadmin -o server -n enabled,enableSSL -v true' && touch ${iib_directory}/iib-${iib_version}/.puppet/mqsichangeproperties_enable_ssl.done",
    creates   => "${iib_directory}/iib-${iib_version}/.puppet/mqsichangeproperties_enable_ssl.done",
    } ->

  exec { 'create_broker_user_admin':
    command   => "su - wmbadmin -c 'source /home/wmbadmin/.bashrc && ${iib_directory}/iib-${iib_version}/server/bin/mqsiwebuseradmin ${boker_name} -c -u ${broker_user_name} -a '${broker_user_password}' -r mqadmin' && touch ${iib_directory}/iib-${iib_version}/.puppet/mqsichangeproperties_user.done",
    timeout   => 1200,
    creates   => "${iib_directory}/iib-${iib_version}/.puppet/mqsichangeproperties_user.done",
    } ->

  exec { 'create_broker_group_admin':
    command   => "su - wmbadmin -c 'source /home/wmbadmin/.bashrc && ${iib_directory}/iib-${iib_version}/server/bin/mqsiwebuseradmin ${boker_name} -c -u ${broker_group_username} -a '${broker_group_password}' -r mqsupport' && touch ${iib_directory}/iib-${iib_version}/.puppet/mqsichangeproperties_group.done",
    timeout   => 1200,
    creates   => "${iib_directory}/iib-${iib_version}/.puppet/mqsichangeproperties_group.done",
    } ->

  exec { 'stop_broker_services':
    command   => "su - wmbadmin -c 'source /home/wmbadmin/.bashrc && ${iib_directory}/iib-${iib_version}/server/bin/mqsistop ${boker_name}' && touch ${iib_directory}/iib-${iib_version}/.puppet/mqsistop_broker.done",
    timeout   => 1200,
    creates   => "${iib_directory}/iib-${iib_version}/.puppet/mqsistop_broker.done",
    } ->

  exec { 'start_broker_services':
    command   => "su - wmbadmin -c 'source /home/wmbadmin/.bashrc && ${iib_directory}/iib-${iib_version}/server/bin/mqsistart ${boker_name}' && touch ${iib_directory}/iib-${iib_version}/.puppet/mqsistart_broker.done",
    timeout   => 1200,
    creates   => "${iib_directory}/iib-${iib_version}/.puppet/mqsistart_broker.done",
    }

   $execution_groups = hiera_hash('profile::generic::iib::server::config_broker::execution_groups', undef)
   $execution_groups.each | $execution_groups | {
   exec { "setup_execution_groups_$execution_groups":
     command   => "su - wmbadmin -c 'source /home/wmbadmin/.bashrc && ${iib_directory}/iib-${iib_version}/server/bin/mqsicreateexecutiongroup ${boker_name} -e ${execution_groups}' && touch ${iib_directory}/iib-${iib_version}/.puppet/mqsistart_setup_${execution_groups}.done",
     timeout   => 1200,
     creates   => "${iib_directory}/iib-${iib_version}/.puppet/mqsistart_setup_${execution_groups}.done",
     require   => Exec['start_broker_services'];
      }
    }

  file { '/home/wmbadmin/broker_db_set.sh':
    mode      => '0554',
    owner     => 'wmbadmin',
    group     => 'wmbadmin',
    backup    => true,
    source    => "/root/cob_setup/broker_db_set.sh",
    require   => Exec['start_broker_services'];
  }

  exec { "setup_exec_set_db":
   command   => "su - wmbadmin -c 'source /home/wmbadmin/.bashrc && /home/wmbadmin/broker_db_set.sh' && touch /home/wmbadmin/db_setup.done",
   timeout   => 1200,
   creates   => "/home/wmbadmin/db_setup.done",
   require   => File['/home/wmbadmin/broker_db_set.sh'];
   }
  }
}

# -----------------------------------------------------------------------------
# End of file.
# -----------------------------------------------------------------------------
